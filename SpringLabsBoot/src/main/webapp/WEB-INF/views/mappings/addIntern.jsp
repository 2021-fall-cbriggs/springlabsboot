<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Add Intern</h1>
<form>
	<div>
		<label for="firstName">First Name</label>
		<input type="text" placeholder="First Name" name="firstName" id="firstName" />
	</div>
	<div>
		<label for="lastName">Last Name</label>
		<input type="text" placeholder="Last Name" name="lastName" id="lastName" />
	</div>
	<div>
		<label for="issuer">Issuer</label>
		<select name="issuer" id="issuer">
			<c:forEach items="${internList}" var="intern">
				<option value="${intern.internId}">${intern.internName}</option>
			</c:forEach>
		</select>
	</div>
	<div>
		<button id="submitBtn" type="button" class="btn btn-primary">Submit</button>
	</div>
</form>

<hr/>

<h2>Interns</h2>
<div id="interns">
	<c:forEach items="${internList}" var="intern">
		${intern} Issuer=${intern.badge.issuer}<br/>
	</c:forEach>
</div>

Intern Result List = ${internResultList}

<script>	
	orly.ready.then(() => {
		orly.qid("submitBtn").addEventListener("click", function(e){
			try {
				e.preventDefault(); // Prevent Form From Submitting "normally" (IE)
				let Intern = {};
				Intern.internName = orly.qid("firstName").value + " " +  orly.qid("lastName").value;
				Intern.issuer = orly.qid("issuer").value;
				
				// Make AJAX call to the server
				fetch("<c:url value='/mappings/addIntern' />", {
				        method: "POST",
				        body: JSON.stringify(Intern),
				        headers: {
				            "Content-Type": "application/json"
				        }
				}).then(function(response) {
					// Get the response, grab the JSON, and return response so we
					// can process it in the next "then" (since this is a Promise...)
				  	if (response.ok) {
				  		console.log("response.status=", response.status);
				  		let message = response.json();
				  		console.log("message = " + message);
				  		return message
				  	} else {
				  		throw new Error("Error: " + response.statusText);
				  	}
				}).then(function(response) {
					let Message = JSON.parse(response);
					let message = Message.message;
					let messageType = Message.messageType;
					
					if (messageType == null || messageType == "undefined" || messageType.length == 0) {
						messageType = "info";
					}
				  	
				  	if (message != null && message != "undefined" && message.length > 0) {
				  		orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});	
				  	}
				  	
				  	updateInternList(Message);
				  	
				}).catch(function(error) {
					let message = 'There was a problem with your fetch operation: ' + error.message;
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:message});
				});
		
			} catch (err) {
				// Do not show try-catch errors to the user in production
				orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:err});
			}
		});
	});
	
	function updateInternList(Message) {
		if (Message != null && Message != "undefined") {
			let list = Message.internList;
			
			if (list != null && list != "undefined" && list.length > 0) {
				let buffer = "";
				for (let Intern of list) {
					buffer += "Intern [internId="+Intern.internId+", internName="+Intern.internName
						+"] Issuer=" + Intern.Badge.issuer + "<br/>";
				}
				orly.qid("interns").innerHTML = buffer;
			}
		}
	}
</script>
