package com.oreillyauto.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the course database table.
 * 
 */
@Entity
@Table(name="course")
@NamedQuery(name="Course.findAll", query="SELECT c FROM Course c")
public class Course implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="course_guid")
	private int courseGuid;

	@Column(name="course_name")
	private String courseName;

	@Column(name="credit")
	private int credit;

	@Column(name="professor_id")
	private int professorId;

	@Column(name="room_id")
	private int roomId;

	//bi-directional many-to-one association to University
	@OneToMany(mappedBy="course", fetch=FetchType.EAGER)
	private List<University> universities;

	public Course() {
	}

    public int getCourseGuid() {
        return courseGuid;
    }

    public void setCourseGuid(int courseGuid) {
        this.courseGuid = courseGuid;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getProfessorId() {
        return professorId;
    }

    public void setProfessorId(int professorId) {
        this.professorId = professorId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public List<University> getUniversities() {
        return universities;
    }

    public void setUniversities(List<University> universities) {
        this.universities = universities;
    }

    @Override
    public String toString() {
        return "Course [courseGuid=" + courseGuid + ", courseName=" + courseName + ", credit=" + credit
                + ", professorId=" + professorId + ", roomId=" + roomId + "]";
    }

}