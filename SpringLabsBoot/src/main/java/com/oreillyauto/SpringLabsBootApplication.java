package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLabsBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLabsBootApplication.class, args);
	}

}
