package com.oreillyauto.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SecurityController {

    @RequestMapping(value = "/home")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String home() {
        return "home";
    }
    
}
